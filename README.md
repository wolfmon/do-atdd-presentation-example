Presentation Sequence:

[demo-1-minium]:Demonstrate initial stage where story is presented to tester.

[demo-1-steps] :Demonstrate stage where tester implments binding to the natrual language scripts.

[demo-1-completed] :Demonstrate final stage in acceptance test where the product passes the test.

[demo-2-added-coupon] :Demonstrate the behavior of pipeline after change in story.

[demo-2-completed] :Demonstrate completed product.


